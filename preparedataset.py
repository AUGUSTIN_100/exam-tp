import pandas as pd

# Charger le fichier CSV
file_name = "reviews_unique.csv"
df = pd.read_csv(file_name, sep=",")

p = df.head()
print(p)
# Supprimer les doublons
df.drop_duplicates(inplace=True)

# Écrire les résultats dans un nouveau fichier
file_name_output = "good_reviews.csv"
df.to_csv(file_name_output, index=False)
