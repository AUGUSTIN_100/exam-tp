
import numpy as np

def test_multiply_arrays():
    # Créez des tableaux de test
    arr1 = np.array([1, 2, 3])
    arr2 = np.array([4, 5, 6])

    # Appellez la fonction à tester
    # multiply_arrays(arr1, arr2)
    result = np.multiply(arr1, arr2)

    # Vérifiez que le résultat est correct
    expected_result = np.array([4, 10, 18])
    np.testing.assert_array_equal(result, expected_result)
