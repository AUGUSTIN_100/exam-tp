# train_model.py

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report
import joblib

# Charger le dataset (remplacez par votre propre fichier CSV)
data = pd.read_csv("good_reviews.csv")

# Séparer les features (X) et la variable cible (y)
X = data.drop(columns=["critiques"])
y = data["labels"]

# Diviser le jeu de données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Instancier le modèle de régression logistique
model = LogisticRegression()

# Entraîner le modèle sur les données d'entraînement
model.fit(X_train, y_train)

# Faire des prédictions sur les données de test
y_pred = model.predict(X_test)

# Évaluer la performance du modèle
report = classification_report(y_test, y_pred)
print("Rapport de classification :\n", report)

# Sauvegarder le modèle entraîné pour une utilisation ultérieure
joblib.dump(model, "modele_logi_regress.pkl")
print("Modèle sauvegardé avec succès !")
