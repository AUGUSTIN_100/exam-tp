from flask import Flask, render_template, request, jsonify
import joblib

app = Flask(__name__)

# Chargez votre modèle entraîné ici (remplacez par votre propre modèle)
model = joblib.load('modele_logi_regress.pkl')

@app.route('/')
def home():
    return "Please use postman to request /predict route"

@app.route('/predict', methods=['POST'])
def predict_sentiment():
    try:
        data = request.get_json()
        review = data['review']

        # Faites la prédiction
        sentiment = model.predict([review])[0]

        # Convertissez la prédiction en "positif" ou "négatif"
        sentiment_label = "positif" if sentiment == 1 else "négatif"

        return jsonify({"sentiment": sentiment_label})
    except Exception as e:
        return jsonify({"error": str(e)})

if __name__ == '__main__':
    app.run(debug=True)
