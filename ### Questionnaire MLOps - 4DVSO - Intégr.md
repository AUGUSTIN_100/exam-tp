### Questionnaire MLOps - 4DVSO - Intégration Continue - M1

#### Fondamentaux DevOps et MLOps

1. **Expliquez ce que signifie DevOps et comment il facilite la collaboration entre les équipes de développement et d'opérations.**
    
    - Le DevOps est la combinaison de Développement et Operations, c'est l'ensemble des méthodes et processus qui visent à faciliter les relations entre la partie dev et Opérations; il facilite les processus d'échange, de communication, la collaboration, de suivi et de mise en production.


2. **Décrivez ce qu'est le MLOps et en quoi il diffère du DevOps, surtout en ce qui concerne le cycle de vie des modèles de machine learning.**

    MLOps, ou Machine Learning Operations, est une approche qui applique les principes du DevOps aux projets de Machine Learning. Comme le DevOps, le MLOps vise à unifier le développement (Dev) et les opérations (Ops) pour améliorer la livraison de valeur et la qualité des systèmes. Voici quelques points clés sur le MLOps:
        
        Développement expérimental :
        Dérive conceptuelle :
        Objectifs communs : 
        Automatisation :  
        Surveillance :  



3. **Expliquez l'acronyme C.A.L.M.S et son importance dans le contexte DevOps**

    Culture
    Automatisation
    Lean
    Mesures
    Sharing

4. **Listez 6 outils couramment utilisés dans la pratique du DevOps et MLOps et décrivez brièvement leur fonction.**

    Gitlab CI/CD : pour creer des pipelines CI/CD
    Azure Pipeline: pour creer des jobs
    Kubernetes: pour orchestrer les solutions déveleoppées
    Docker : Outils de contenarisation
    Sonarcloud: outils cloud d'analyse de code
    Grafana: outils graphique de suivi des pods (Kubernetes)

5. **Expliquez ce qu'est l'intégration continue et son rôle.**

    L'**intégration continue (CI)** est une pratique **DevOps** qui consiste à **régulièrement intégrer les modifications** apportées dans un dépôt de code. Voici quelques points clés sur l'intégration continue :

    L'objectif principal de la CI est de **détecter les problèmes d'intégration au plus tôt** lors du développement. Elle permet également d'**automatiser l'exécution des suites de tests** et de suivre l'évolution du développement du logiciel ².

    Une fois que votre outil d'intégration continue est connecté à votre dépôt de code (par exemple, via un webhook), il exécute des tâches sur les différents commits du dépôt. Ces tâches sont déclenchées sous certaines conditions. L'intégration continue peut être utilisée pour n'importe quel type de projet de développement, qu'il s'agisse d'applications déployées sur du bare-metal, des VM ou des applications cloud natives déployées via des conteneurs avec Docker et Kubernetes ¹.



6. **La livraison continue (Continuous Delivery, CD)** est une pratique de développement logiciel qui vise à automatiser le processus de déploiement d'une application. Elle permet de livrer fréquemment et de manière fiable des mises à jour logicielles aux utilisateurs finaux. Dans le contexte des workflows MLOps, la livraison continue s'intègre en automatisant le déploiement des modèles d'apprentissage automatique (AA) dans des environnements de production. Cela garantit que les nouvelles versions de modèles sont déployées rapidement et sans erreur.

7. **Le déploiement continu** est un sous-ensemble de la livraison continue. Il se concentre spécifiquement sur l'automatisation du déploiement des applications ou des modèles dans des environnements de production. Contrairement à la livraison continue, qui peut inclure des étapes manuelles de validation, le déploiement continu automatise complètement le processus de mise en production.

8. **Le versionnage sémantique** est une méthode courante de numérotation des versions de logiciels. Elle utilise trois numéros pour chaque version : majeure, mineure et correctif. Par exemple, la version 2.1.3 indique une version majeure 2, une version mineure 1 et trois correctifs. Le versionnage sémantique est important dans la gestion des versions de logiciels car il permet aux utilisateurs et aux fournisseurs de suivre les différentes versions publiées par l'entreprise de manière méthodique.

9. Quatre fournisseurs de services cloud et leurs caractéristiques uniques :
    - **Amazon Web Services (AWS)** : Offre une large gamme de services cloud, notamment des capacités de calcul, de stockage, d'analyse de données et d'apprentissage automatique.
    - **Microsoft Azure** : Intègre des services cloud avec des outils de développement, de gestion et de sécurité. Il est également compatible avec les technologies Microsoft existantes.
    - **Google Cloud Platform (GCP)** : Met l'accent sur l'IA et l'apprentissage automatique, avec des services tels que BigQuery et TensorFlow.
    - **IBM Cloud** : Propose des services cloud hybrides, permettant aux entreprises de combiner des ressources locales et cloud.

10. **Infrastructure as a Service (IaaS)** : L'IaaS offre des ressources de calcul, de stockage et de mise en réseau essentielles à la demande, sur une base de paiement à l'utilisation. Par exemple, Amazon EC2 et Microsoft Azure sont des services IaaS qui permettent de provisionner rapidement des machines virtuelles pour exécuter des applications.

11. **Platform as a Service (PaaS)** : Le PaaS fournit une plateforme complète pour le développement, le déploiement et la gestion d'applications. Un exemple est Google App Engine, qui permet aux développeurs de créer et de déployer des applications sans se soucier de l'infrastructure sous-jacente.

13. **Software as a Service (SaaS)** : Le SaaS propose des applications logicielles hébergées dans le cloud et accessibles via un navigateur web. Un exemple courant est Microsoft Office 365, qui offre des applications telles que Word, Excel et Outlook en tant que service.

14. **Data Version Control (DVC)** : DVC est un outil open source qui permet de gérer les versions de données et de modèles dans les projets d'apprentissage automatique. Une commande DVC courante est `dvc add`, qui ajoute un fichier de données ou de modèle au suivi de version.

15. **Un training job** est le processus d'entraînement d'un modèle d'apprentissage automatique. Il consiste à alimenter le modèle avec des données d'entraînement, à ajuster ses paramètres et à évaluer sa performance jusqu'à ce qu'il atteigne un niveau de précision souhaité. Le training job est essentiel pour créer des modèles performants et précis.
