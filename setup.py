from setuptools import setup, find_packages

setup(
    name='ci_mlop',
    version='1.0.0',
    packages=find_packages(),
    install_requires=[
        # your regular dependencies here
    ],
    extras_require={
        'test': [
            'pytest',
            'numpy',
        ]
    },
)
